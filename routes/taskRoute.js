
const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();
// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController");


router.get("/", (req, res) => {

	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	// "resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name for a result using the parameter name "result"/"res"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

})


router.post("/", (req, res) => {

	// The "createTask" function needs the data from the request body, so we need to supply it to the function
	// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	
})


router.delete("/:id", (req, res) => {

	
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

})


router.put("/:id", (req, res) => {

	
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

})

router.get("/:id", (req, res) => {

	
	taskController.gettingSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));

})

router.put("/:id/complete", (req, res) => {

	
	taskController.changeStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

})



module.exports = router;