const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client/Postman
		name : requestBody.name
	})
	return newTask.save().then((task, error) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if (error) {
			console.log(error);
			return false;
		// Save successful, returns the new task object back to the client/Postman
		} else {
			return task; 
		}
	})
}

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.deleteTask = (taskId) => {	
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return false;
		// Delete successful, returns the removed task object back to the client/Postman
		} else {
			if(removedTask === null || removedTask === ''){
				return false
			}else{
				return removedTask
			}
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {	
	return Task.findById(taskId).then((result, error) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){
			console.log(err);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr){
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				console.log(saveErr);
				return false;
			// Update successful, returns the updated task object back to the client/Postman
			} else {
				/// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return updatedTask;
			}
		})
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			// Update successful, returns the updated task object back to the client/Postman
			} else {
				return updatedTask;
			}
		})
	})
}
module.exports.gettingSpecificTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((result, err) => {	
		if(err){
			console.log(err);
			return false;
		} else {
			return result;
		}
	})
}

module.exports.changeStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err);
			return false;
		}
		result.status = "complete";
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {			
				return updatedTask;
			}
		})
	})
}